from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient as AWS    #To connect to AWS IOT
from groundobjects.groundstation import GroundStation
import json
import time


#aws stuff
myAWS = AWS("mqttClient") #only this client id will be able to connect so do not change
enpoint = "a23fpg12m7w3rv-ats.iot.eu-central-1.amazonaws.com"
portAWS = 8883
myAWS.configureEndpoint(enpoint, portAWS)
myAWS.configureCredentials("AmazonRootCA1.pem", "3fcca91984-private.pem.key", "3fcca91984-certificate.pem.crt")
print("aws configured...")


#Starts connection to AWS
myAWS.connect()
print("aws connected...")


topics_sub = ['schedule']
station = GroundStation(uid = 'e02fafcc-17e6-4e1f-ab95-c7857fb66290', name= 'shang_siset_uhf', latitude= 45.733, longitude=126.627)


# callback sends data to client side
def alertCallback(client, userdata, message):
    # receive the alert from server; and extract your schedule
    print("received {} on topic {}".format(message.payload, message.topic))
    topicData = json.loads(message.payload)
    print(topicData)

    # update the schedule
    extractStnSchedule(topicData)

# subscribe to topic with callback as events
myAWS.subscribe(topics_sub[0], 1, alertCallback)


def extractStnSchedule(schedule):
    global station
    mySchedule = schedule[station.name]
    print(mySchedule)

#keep listening
while True:
    time.sleep(1)

