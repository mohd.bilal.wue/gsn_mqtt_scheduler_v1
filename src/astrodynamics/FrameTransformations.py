"""
Created on Jul 3, 2019

@author: mob33uk
"""
import numpy as np
from astrodynamics.Rotations import *
from astrodynamics.Time import *


def eci2ecef(r_eci, v_eci, a_eci, tt_cent, jd_ut1, lod, pmc_x, pmc_y, eqeterms, ddpsi, ddeps):
    """convert a vector in eci frame to ecef frame
    parameters: r_eci     - position vector in eci frame                                    - km
                v_eci     - velocity vector in eci frame                                    - km/s
                a_eci     - acceleration vector in eci frame                                - km/s^2
                tt_cent   - julian centuries of terrestrial time                            -centuries
                jd_ut1    - julian date of Universal Time 1 (UT1 = UTC + delta_UT1)         -days from 4713 BC
                lod       - excess length of day                                            - seconds
                pmc_x     - polar motion coefficient                                        -arc sec
                pmc_y     - polar motion coefficient                                        -arc sec
                eqeterms  - terms for ast calculation                                       -0,2
                ddpsi     - delta psi correction to gcrf                                    -rad
                ddeps     - delta eps correction to gcrf                                    -rad

    return:    recef      - position vector earth fixed                                     -km
               vecef      - velocity vector earth fixed                                     -km/s
               aecef      - acceleration vector earth fixed                                 -km/s^2
    """
    return None


def eci2geodectic(vec_eci, lst, latitude):
    """convert a vector in eci frame to geodectic frame
    parameters: vec_eci        - vector in eci frame            -any units: type- numpy.array
                latitude       - latitude of the location       -deg
                lst            - local sidereal time            -deg
                time           - time when to convert           -UTC: type- datetime.datetime
    return:     range, azimuth, elevation
    assumption: earth is a sphere of constant radius
    """
    theta = math.radians(lst)
    phi = math.radians(latitude)
    vec_sez = np.matmul(np.matmul(rot2(math.pi / 2 - phi), rot3(theta)), vec_eci)
    return vec_sez
