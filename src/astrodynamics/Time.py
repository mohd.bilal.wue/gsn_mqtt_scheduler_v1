from dateutil.tz import UTC
def UTC2JD(UTC):
    '''calculates the Julian Day given UTC
    parameters: UTC - dateUTC.dateUTC
    return : Julian day i.e., days from 4713 BC
    '''
    JD = 367*UTC.year - int(7*(UTC.year + int((UTC.month + 9)/12) )/4) +int(275*UTC.month/9) + UTC.day + 1721013.5 + ((UTC.second / 60.0 + UTC.minute) / 60.0 + UTC.hour) / 24.0
    return JD

def getJulianCenturies(JD):
    '''calculate number of centures of JD
    '''
    return (JD-2451545)/36525
    
def getGMST(t_cent):
    '''returns Greenwich Mean Sidereal Time given the centuries of the Julian Day of the UTC
    values returned in degrees'''
    theta_gmst = 67310.54841 + (876600*3600 + 8640184.812866)*t_cent + 0.093104*(t_cent**2) -6.2*1e-6*(t_cent**3) 
    theta_gmst = theta_gmst%86400 - 86400
    theta_gmst_deg = theta_gmst/240 
    if theta_gmst<0:
        theta_gmst_deg = 360  + theta_gmst_deg
    return theta_gmst_deg