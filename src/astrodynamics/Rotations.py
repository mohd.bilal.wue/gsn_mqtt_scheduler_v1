import numpy as np
import math
def rot1(theta):
    rotmat = np.array([[1,0,0],[0,math.cos(theta),math.sin(theta)], [0,-math.sin(theta),math.cos(theta)]])
    return rotmat

def rot2(theta):
    rotmat = np.array([[math.cos(theta),0,-math.sin(theta)],[0,1,0], [math.sin(theta),0,math.cos(theta)]])
    return rotmat

def rot3(theta):
    rotmat = np.array([[math.cos(theta),math.sin(theta),0],[-math.sin(theta),math.cos(theta),0], [0,0,1]])
    return rotmat