from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient as AWS    #To connect to AWS IOT
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import datetime
import json, time, os
from observation.satellite import Satellite
from observation.tle_loader import loadTLE
from scheduler.ObjectiveFunction import WeightedSchedulingObjective as wsObjective
from scheduler.GeneticAlgorithm import GeneticAlgorithm as ga
import plotly.figure_factory as plotlyff
from spaceobjects.satellite import Satellite
from groundobjects.groundstation import GroundStation
from scheduler.ContactWindowsVector import ContactWindowsVector as CWVector
from database.models import Schedule
import uuid
import shelve

# this server has access to the database. the reason being that it needs to know the satellites and gs combinations
# this could have been sent by the webapp but in case the server stops, it needs to be initiated with the previous state
# Of course it could be made such that the data is somehow stored locally and on re-start
# we load the whole data from local files, but this may not be a safe option, but definitely faster and less complex.

#database configuration
engine = create_engine(os.getenv("TIMGSN_DB_URL_TEST"))
db = scoped_session(sessionmaker(bind=engine))
# Session = sessionmaker(bind=engine)
# session = Session()

#topics list
topics_sub = ['alerts/mqttserver/webapp']
topics_pub = ['schedule']
#satellites=[] #this can be used to save the state temporarily

#aws stuff
myAWS = AWS("mqttServer") #only this client id will be able to connect so do not change
enpoint = "a23fpg12m7w3rv-ats.iot.eu-central-1.amazonaws.com"
portAWS = 8883
myAWS.configureEndpoint(enpoint, portAWS)
myAWS.configureCredentials("AmazonRootCA1.pem", "3410122e45-private.pem.key", "3410122e45-certificate.pem.crt")
print("aws configured...")

#Starts connection to AWS
myAWS.connect()
print("aws connected...")

tle_update_time = 0

#callback sends data to client side
def alertCallback(client, userdata, message):
    #receive the alert from webapp; process the info
    print("received {} on topic {}".format(message.payload, message.topic))
    topicdata = json.loads(message.payload)
    print(topicdata)
    
    #update the schedule
    updateSchedule(topicdata)

#subscribe to topic with callback as events
myAWS.subscribe(topics_sub[0], 1, alertCallback)


def updateSchedule(data):
    global tle_update_time
    print("updating schedule with new information...")
    # query for all the approved requests
    requests = getRequests()
    #convert requests to a list(array here) of contact windows
    cwVector  = req2ContactWindows(requests)
    plotSchedule(cwVector)

    # perform scheduling
    objectiveFx = wsObjective(cwVector=cwVector, stnFairWt=0.5, satFairWt=0.5)
    nVars = len(cwVector.observations)
    gaSolver = ga(objectiveFx, popSize=50, nVars=nVars, maxGen=100)
    population, bestSol = gaSolver.simulate()
    bestChromosome = bestSol[0]
    bestObservations = []
    for i in range(nVars):
        if bestChromosome[0,i]:
            bestObservations.append(cwVector.observations[i])
    cwVectorBest = CWVector(bestObservations)

    #remove conflicts
    cwVectorBest.sortByTime()
    satConflicts = cwVectorBest.getSatConflicts()
    gsConflicts = cwVectorBest.getGsConflicts()
    unionConflicts = list(set().union(satConflicts,gsConflicts))
    bestObservationsNew = bestObservations
    for conflictPass in unionConflicts:
        if conflictPass in bestObservationsNew:
            print('found conflicting pass in observations')
            bestObservationsNew.remove(conflictPass)
    cwVectorBestNew = CWVector(bestObservationsNew)
    plotSchedule(cwVectorBestNew)
    # publish the schedule

    schedule={}

    for overpass in bestObservationsNew:
        scheduledPass = {}
        scheduleEntry = saveSchedule2DB(overpass)
        scheduledPass["UID"] =scheduleEntry.uid
        scheduledPass["norad"] = overpass.satellite.norad
        scheduledPass["startTime"] = str(scheduleEntry.start_time)
        scheduledPass["endTime"] = str(scheduleEntry.end_time)
        scheduledPass["name"] = scheduleEntry.name

        if overpass.station.name in schedule:
            schedule[overpass.station.name].append(scheduledPass)
        else:
            schedule[overpass.station.name] = [scheduledPass]
    print(schedule)
    pub_payload= json.dumps(schedule)   
    print(pub_payload)             
    myAWS.publish(topics_pub[0],pub_payload,0)

def saveSchedule2DB(overpass):
    '''saves the generated schedule to the timgsn database which can then be loaded by the web application'''
    uid = overpass.station.uid + str(overpass.satellite.norad) + str(uuid.uuid4())
    name = overpass.station.name + str(overpass.satellite.norad) + str(overpass.startTime) + str(overpass.endTime)
    starttime = str(overpass.startTime)
    endtime = str(overpass.endTime)
    satellite = db.execute("SELECT * FROM satellites WHERE norad=:norad", {"norad": overpass.satellite.norad}).fetchone()
    gs = db.execute("SELECT * FROM groundstations WHERE gsn_name=:gsn_name", {"gsn_name": overpass.station.name}).fetchone()
    schedule = Schedule(uid=uid, name=name, start_time=starttime, end_time=endtime, satellite_id=satellite.id, gs_id=gs.id, norad=satellite.norad, station_name=gs.gsn_name)
    #check if schedule already exists in db: if not add it else do nothing!
    scheduleDB = db.execute("SELECT * FROM schedule WHERE name=:name",{"name":schedule.name}).fetchone()
    if scheduleDB is None:
        db.add(schedule)
        db.commit()
    return schedule

def getRequests():
    '''
    get list of approved requests from the database. Approved implies that the receiving operator has approved the req
    :return: list of approved requests
    '''
    # fetch only the approved requests
    reqStatus = True
    requests = db.execute("SELECT * FROM trackrequests WHERE status=:status",{"status":reqStatus}).fetchall()
    return requests


def req2ContactWindows(requests):
    '''From requests create contact windows
    :parameter: list of requests
    '''
    # calculate the times
    # for test purposes let the start and end time be between now and 3 days from now
    timeVector = []
    tStart = datetime.datetime.utcnow()
    tEnd = tStart + datetime.timedelta(days=3)
    observations =[]
    for req in requests:
        satID = req.satellite_id
        gsID = req.gs_id
        satDB = db.execute("SELECT * FROM satellites WHERE id=:id", {"id":satID}).fetchone()
        gsDB = db.execute("SELECT * FROM groundstations WHERE id=:id", {"id":gsID}).fetchone()
        print("calculating overpasses for sat:{}".format(satDB.norad))
        satObj = Satellite(satDB.norad)
        gsObj = GroundStation(gsDB.uid, gsDB.gsn_name, gsDB.latitude, gsDB.longitude)
        overpasses = satObj.getOverpasses(station=gsObj, time_span=[tStart, tEnd])
        # for overpass in overpasses:
        #     print("overpass {}, start_time {}, end_time {}".format(overpass, overpass.startTime, overpass.endTime))
        if overpasses:
            for overpass in overpasses:
                observations.append(overpass)
    print(observations)
    cwVector = CWVector(observations)
    # gsContacts = cwVector.getGsContacts()
    return cwVector



def plotSchedule(cwVector):
    '''Plot schedule
    :parameter: cwVector: vector containing all the requests'''
    observations = cwVector.observations
    plotDataGs = []
    plotDataSat = []
    for overpass in observations:
        obsDictGs = dict(Task= overpass.station.name, Start=str(overpass.startTime), Finish =str(overpass.endTime), Resource=str(overpass.satellite.norad))
        obsDictSat = dict(Task= overpass.satellite.norad, Start=str(overpass.startTime), Finish =str(overpass.endTime), Resource=str(overpass.station.name))
        plotDataGs.append(obsDictGs)
        plotDataSat.append(obsDictSat)

    figGs = plotlyff.create_gantt(plotDataGs,  index_col='Resource', show_colorbar=True, group_tasks=True)
    figGs.show()

    figSat = plotlyff.create_gantt(plotDataSat,  index_col='Resource', show_colorbar=True, group_tasks=True)
    figSat.show()


# updateSchedule([]) #use this for testing only
# Loop forever
while True:
    time.sleep(1)