from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import os

engine = create_engine(os.getenv("TIMGSN_DB_URL_TEST"))
db = scoped_session(sessionmaker(bind=engine))

