from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Float, Boolean, Text, String, DateTime, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker, relationship
import os, json
from flask import Flask
from flask_bcrypt import Bcrypt
from time import time
app = Flask(__name__)
bcrypt = Bcrypt(app)

engine = create_engine(os.getenv("TIMGSN_DB_URL_TEST"))
db = scoped_session(sessionmaker(bind=engine))


import datetime

Base = declarative_base()


class Client(Base):
    __tablename__ = "gsclients"
    id = Column(Integer, primary_key=True)
    uid = Column(String(100), unique=True, nullable=False)
    username = Column(String(100), unique=True, nullable=False)
    email = Column(String(100), unique=True, nullable=False)
    password = Column(Text, nullable=False)
    last_request_read_time = Column(DateTime, default=datetime.datetime.utcnow)
    last_response_read_time = Column(DateTime, default=datetime.datetime.utcnow)
    last_message_read_time = Column(DateTime, default=datetime.datetime.utcnow)
    requests_sent = relationship("Request", foreign_keys="Request.sender_id", backref="sender_req", lazy='dynamic')
    requests_received = relationship("Request", foreign_keys="Request.recipient_id", backref='recipient_req',
                                        lazy='dynamic')
    responses_sent = relationship("Response", foreign_keys="Response.sender_id", backref="sender_res",
                                     lazy='dynamic')
    responses_received = relationship("Response", foreign_keys="Response.recipient_id", backref='recipient_res',
                                         lazy='dynamic')
    messages_sent = relationship("Message", foreign_keys="Message.sender_id", backref="sender", lazy='dynamic')
    messages_received = relationship("Message", foreign_keys="Message.recipient_id", backref="recipient",
                                        lazy='dynamic')
    notifications = relationship("Notification", backref="user", lazy='dynamic')

    #
    def __init__(self, uid, username, email, password):
        self.uid = uid
        self.username = username
        self.email = email
        self.password = bcrypt.generate_password_hash(password)
        self.last_req_read_time = datetime.datetime(1900, 1, 1)
        self.last_res_read_time = datetime.datetime(1900, 1, 1)
        self.last_message_read_time = datetime.datetime(1900, 1, 1)

    def check_password(self, password):
        return bcrypt.check_password_hash(self.password, password)

    def new_requests(self):
        last_read_time = self.last_request_read_time or datetime.datetime(1900, 1, 1)
        return Request.query.filter_by(recipient_req=self).filter(Request.timestamp > last_read_time).count()

    def new_responses(self):
        last_read_time = self.last_response_read_time or datetime.datetime(1900, 1, 1)
        return Response.query.filter_by(recipient_res=self).filter(Response.timestamp > last_read_time).count()

    #
    def new_messages(self):
        last_read_time = self.last_message_read_time or datetime.datetime(1900, 1, 1)
        return Message.query.filter_by(recipient=self).filter(Message.timestamp > last_read_time).count()

    def send_request(self, uid, sender, recipient, satid, gsid, body):
        req = Request(uid=uid, sender_req=sender, recipient_req=recipient, satellite_id=satid, gs_id=gsid, body=body)
        db.add(req)
        db.commit()
        return req

    def send_response(self, uid, request_id, sender, recipient, body):
        res = Response(uid=uid, request_id=request_id, sender_res=sender, recipient_res=recipient, body=body)
        db.add(res)
        db.commit()
        return res

    def send_message(self, sender, recipient, body):
        msg = Message(sender=sender, recipient=recipient, body=body)
        db.add(msg)
        db.commit()
        return msg

    def add_notification(self, name, data):
        self.notifications.filter_by(name=name).delete()
        n = Notification(name=name, payload_json=json.dumps(data), user=self)
        db.add(n)
        db.commit()
        return n

class Groundstation(Base):
    __tablename__ = "groundstations"
    id = Column(Integer, primary_key=True)
    uid = Column(String(100), unique=True, nullable=False)
    gsn_name = Column(String(100), unique=True)
    owner_id = Column(Integer, ForeignKey("gsclients.id"), nullable=False)
    longitude = Column(Float())
    latitude = Column(Float())
    height = Column(Float())
    elmin = Column(Float())
    band_rx = Column(String(100))
    band_tx = Column(String(100))
    freq_rx = Column(Float())
    freq_tx = Column(Float())
    antenna_bw_rx = Column(Float())
    antenna_bw_tx = Column(Float())
    pwr_rx = Column(Float())
    pwr_tx = Column(Float())
    antenna_type = Column(String(100))
    antenna_gain_rx = Column(Float())
    antenna_gain_tx = Column(Float())
    antenna_eirp = Column(Float())
    ant_plr_rx = Column(String(100))
    requests = relationship("Request", foreign_keys="Request.gs_id", backref="station", lazy='dynamic')


class Satellite(Base):
    __tablename__ = "satellites"
    id = Column(Integer, primary_key=True)
    norad = Column(Integer, unique=True, nullable=False)
    requests = relationship("Request", backref="satellite", lazy=True)


# define gs and client joining table
class Gsclrel(Base):
    __tablename__ = "gsclientrelations"
    id = Column(Integer, primary_key=True)
    gs_id = Column(Integer, ForeignKey("groundstations.id"), nullable=False)
    client_id = Column(Integer, ForeignKey("gsclients.id"), nullable=False)


# define sat and client joining table
class Satclrel(Base):
    __tablename__ = "satclientrelations"
    id = Column(Integer, primary_key=True)
    sat_id = Column(Integer, ForeignKey("satellites.id"), nullable=False)
    client_id = Column(Integer, ForeignKey("gsclients.id"), nullable=False)


# request for tracking
class Request(Base):
    __tablename__ = "trackrequests"
    id = Column(Integer, primary_key=True)
    uid = Column(String(100), unique=True, nullable=False)
    sender_id = Column(Integer, ForeignKey("gsclients.id"), nullable=False)
    recipient_id = Column(Integer, ForeignKey("gsclients.id"), nullable=False)
    satellite_id = Column(Integer, ForeignKey("satellites.id"), nullable=False)
    gs_id = Column(Integer, ForeignKey("groundstations.id"), nullable=False)
    body = Column(Text)
    timestamp = Column(DateTime, index=True, default=datetime.datetime.utcnow())
    status = Column(Boolean, default=False, nullable=False)

    def updateState(self, state_des):
        self.status = state_des
        db.commit()

    def __repr__(self):
        return '<Request {}>'.format(self.body)


# response to requests
class Response(Base):
    __tablename__ = "trackresponses"
    id = Column(Integer, primary_key=True)
    uid = Column(String(100), unique=True, nullable=False)
    request_id = Column(Integer, ForeignKey("trackrequests.id"), nullable=False)
    sender_id = Column(Integer, ForeignKey("gsclients.id"), nullable=False)
    recipient_id = Column(Integer, ForeignKey("gsclients.id"), nullable=False)
    body = Column(Text)
    timestamp = Column(DateTime, index=True, default=datetime.datetime.utcnow())

    def __repr__(self):
        return '<Response {}>'.format(self.body)


#  chat messages
class Message(Base):
    __tablename__ = "messages"
    id = Column(Integer, primary_key=True)
    sender_id = Column(Integer, ForeignKey("gsclients.id"))
    recipient_id = Column(Integer, ForeignKey("gsclients.id"))
    body = Column(Text)
    timestamp = Column(DateTime, index=True, default=datetime.datetime.utcnow)

    def __repr__(self):
        return '<Message {}>'.format(self.body)


# notifications
class Notification(Base):
    __tablename__ = "notifications"
    id = Column(Integer, primary_key=True)
    name = Column(String(128), index=True)
    user_id = Column(Integer, ForeignKey("gsclients.id"))
    timestamp = Column(Float, index=True, default=time)
    payload_json = Column(Text)

    def get_data(self):
        return json.loads(str(self.payload_json))


class Schedule(Base):
    __tablename__ = 'schedule'
    id = Column(Integer, primary_key=True)
    uid = Column(String(100), unique=True, nullable=False)
    name = Column(String(100))
    start_time = Column(DateTime, index=True, default=datetime.datetime.utcnow)
    end_time = Column(DateTime, index=True, default=datetime.datetime.utcnow)
    satellite_id = Column(Integer, ForeignKey("satellites.id"), nullable=False)
    gs_id = Column(Integer, ForeignKey("groundstations.id"), nullable=False)
    norad = Column(Integer, unique=True, nullable=False)
    station_name = Column(String(100), unique=True)


Base.metadata.create_all(engine)