import numpy as np
import random
from itertools import permutations
import logging

class GeneticAlgorithm():
    '''Implements a genetic algorithm
    :parameter: objective function to be evaluated
    :parameter: initPop- vector of initial population
    '''
    def __init__(self, objectiveFunction, initPop = None, popSize = 50, nVars = 1, maxGen = 100, lb=None, ub=None, intCon = None ):
        self.objectiveFunction = objectiveFunction
        self.popSize = popSize
        self.nVars = nVars
        self.maxGen = maxGen
        self.initPop = initPop
        self.lb = lb
        self.ub = ub
        self.intCon = intCon
        self.populationHistory = []
        self.bestFitnessHistory =[]
        self.averageFitnessHistory = []
        if self.initPop is None:
            self.population = np.zeros((self.popSize, self.nVars))
            self.generateInitialPopulation()
        else:
            self.initPop = initPop
            self.population = initPop

    def setObjectiveFunction(self, objectiveFunction):
        self.objectiveFunction = objectiveFunction

    def getObjectiveFunction(self):
        return self.objectiveFunction

    def generateInitialPopulation(self):
        ''' generate initial population as zeros and ones. Ideally ask for LB and UB as well as any integer constraints
        '''
        for i in range(self.popSize):
            chromosome = np.zeros((1,self.nVars))
            for k in range(self.nVars):
                chromosome[0,k] = random.randrange(0,2)
            self.population[i,:] = chromosome

    def calcFitness(self, chromosome):
        # NOTE: objective function must do the necessary transformation from binary vector to problem specific data
        return self.objectiveFunction.evaluate(chromosome)

    def rouletteSelection(self, population, poolSize):
        '''Selects randomly chromosomes fit for mating using roulette selection'''
        fitness = np.zeros((1, self.popSize))
        for i in range(self.popSize):
            fitness[0,i] = self.calcFitness(population[i,:])
        #normalize fitness and scale to number of times a chromosome should be duplicated in the pool
        nChromCopies = np.ceil(fitness*poolSize/sum(sum(fitness)))
        # random.choices implements roulette selection where k defines the number of spins
        rouletteInd = random.choices(np.arange(0,self.popSize), weights=nChromCopies.transpose(), k=self.popSize)
        parents = population[np.unique(rouletteInd), :]
        return parents

    def selectParents(self, parentSize):
        '''selects a combination of parents from the number of fit parents'''
        parent1 = random.randrange(0, parentSize)
        parent2 = random.randrange(0, parentSize)
        if parent1==parent2:
            parent2 = random.randrange(0, parentSize)
        return [parent1, parent2]

    def mate(self, parent1, parent2):
        '''create children by random crosssover
        :parameter: parents
        :returns: list of offspring (each an array)'''
        sliceIndex = random.randrange(0, self.nVars)
        offspring1 = np.zeros((1, self.nVars))
        offspring2 = np.zeros((1, self.nVars))
        offspring1[0,0:sliceIndex]  = parent1[0,0:sliceIndex]
        offspring1[0,sliceIndex:]  = parent2[0,sliceIndex:self.nVars]
        offspring2[0, 0:sliceIndex] = parent2[0, 0:sliceIndex]
        offspring2[0, sliceIndex:] = parent1[0, sliceIndex:self.nVars]
        return [offspring1, offspring2]

    def mutate(self, chromosome):
        # select randomly how many genes to mutate
        nGeneMutate = random.randrange(0,self.nVars)
        # select the indices of the genes to be mutated randomly, a list is generated
        indGeneMutate  = random.choices(np.arange(0,self.nVars),k=nGeneMutate)
        #mutate
        for ind in indGeneMutate:
            if chromosome[0,ind] ==0:
                chromosome[0, ind] = 1
            else:
                chromosome[0, ind] = 0
        return chromosome

    def surviveFittest(self, population):
        '''trims the population to the best chromosomes, the number set by self.popSize
        :parameter: a matrix of population
        :return: a population of size = popSize sorted by increasing population'''
        popSizeCurr = population.shape[0]
        fitness = np.zeros((popSizeCurr,1))
        for i in range(popSizeCurr):
            fitness[i,0] = self.calcFitness(population[i,:])
        popAug = np.hstack((population, fitness))
        popNew = popAug[popAug[:,-1].argsort(),:-1]
        logging.debug('current population sorted by fitness is \n {}'.format(popAug[popAug[:,-1].argsort(),:]))
        popFit = popNew[population.shape[0] - self.popSize:,: ]
        return popFit

    def getBestChromosome(self, population):
        '''finds the best chromosome in a population
        :parameter: matrix of population
        :returns: list of best chromosome (array), best fitness value and average fitness of the population'''
        popSizeCurr = population.shape[0]
        fitness = np.zeros((popSizeCurr,1))
        for i in range(popSizeCurr):
            fitness[i,0] = self.calcFitness(population[i,:])
        popAug = np.hstack((population, fitness))
        popNew = popAug[popAug[:,-1].argsort(),:]
        bestChrom = popNew[[popSizeCurr-1],:-1]
        bestFitness = popNew[popSizeCurr-1,-1]
        avgFitness = np.average(fitness)
        return [bestChrom, bestFitness, avgFitness]

    def simulate(self):
        '''main part of the GA simulation wherein the populations are updated as per the number of max generations set
        :parameter: None
        :returns: latest population after the simulation, best chromosome (individual)'''

        #define a pool size for parent selection
        poolSize = self.popSize*100
        for i in range(self.maxGen):
            # select parents using roulette selection
            parents = self.rouletteSelection(self.population, poolSize)
            # self.population is updated in loop, assign local variable to it
            populationUpdated = self.population
            # create combinations of parents from the population: check: should one parent be allowed to have only one
            # partner?
            parentComb = permutations(np.arange(parents.shape[0]),2)

            # for each parent combination, create children by crossover, then mutate them, and add to population
            for couple in parentComb:
                offsprings = self.mate(parents[[couple[0]],:], parents[[couple[1]],:])
                offspring1 = self.mutate(offsprings[0])
                offspring2 = self.mutate(offsprings[1])
                populationUpdated = np.vstack((populationUpdated, offspring1))
                populationUpdated = np.vstack((populationUpdated, offspring2))
            # keep only the best chromosomes
            popSurvived = self.surviveFittest(populationUpdated)
            #update self.population
            self.population = popSurvived
            self.populationHistory.append(popSurvived)
            currentSolution = self.getBestChromosome(popSurvived)
            self.bestFitnessHistory.append(currentSolution[1])
            self.averageFitnessHistory.append(currentSolution[2])
        return (popSurvived, self.getBestChromosome(popSurvived))




























        
        
    