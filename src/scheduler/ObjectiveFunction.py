import math
from scheduler.ContactWindowsVector import ContactWindowsVector as CWVector
import logging
import numpy as np

class WeightedSchedulingObjective():
    '''This class implements an objective function that weights the different objectives as desired by the user. 
    The different objectives are: maximizing priority, maximizing matching of visibility windows
    of satellite, minimizing the clashes of different satellite to ground stations, max communication time of s/c with gs, and
    reducing the idle time of ground stations.
    '''
    def __init__(self, cwVector, priorityWt=0.0, stnFairWt=0.0, satFairWt = 0.0, stnUsageWt = 0.0):
        self.cwVector = cwVector
        self.priority = 0.0
        self.fairStations = 0.0
        self.fairSatellites = 0.0
        self.usageStations = 0
        self.value = 0.0
        self.priorityWt = priorityWt
        self.stnFairWt = stnFairWt
        self.satFairWt = satFairWt
        self.stnUsageWt = stnUsageWt
        
    def setObjectiveWeights(self,objectiveWeights):
        self.objectiveWeights=objectiveWeights
        
    def getObjectiveWeights(self):
        return self.objectiveWeights
    
    def getValue(self):
        return self.value

    def calcFairness(self, nrOfContactsPerObject):
        '''calculates fairness for a set of objects (ground/space).
        :parameter: A list of number of contacts per object for all the objects for which fairness is to be calculated
        :returns: Jain's Fairness Index = sigma(contactsPerObj)^2/(nrOfObjects*sigma(contactsPerObj^2))'''

        nObjects = len(nrOfContactsPerObject)
        jainNumerator = 0.0
        jainDenominator = 0.0
        # calculate numerator and denominator for Jain's index
        for i in range(nObjects):
            jainNumerator +=  nrOfContactsPerObject[i]
            jainDenominator +=  math.pow(nrOfContactsPerObject[i],2)
        # calculate Jain's index
        jainFairness = math.pow(jainNumerator, 2)/(nObjects*jainDenominator)
        return jainFairness

    def evaluate(self, binarySelector = None):
        '''evaluates the value of the objective function as the weighted sum priority, fairness of stations, fairness of satellites,...
        equal distribution of station usage
        :parameter: list of contact windows as requested at the time of evaluation
        :returns: a mean between 0 and 1. '''
        if binarySelector is None:
            binarySelector =  np.ones(len(self.cwVector.observations))
        observations = self.cwVector.observations
        newObservations = []

        for i in range(len(binarySelector)):
             if binarySelector[i]==1:
                 newObservations.append(observations[i])
        logging.debug('{}'.format(newObservations))
        cwVector = CWVector(newObservations)
        satellites = cwVector.getSatellites()
        stations = cwVector.getStations()
        satContacts  = cwVector.getSatelliteContacts()
        stationContacts = cwVector.getGsContacts()
        nrOfContactsPerSat =[]
        nrOfContactsPerStn = []

        for sat in satellites:
            nrOfContactsPerSat.append(len(satContacts[str(sat.norad)]))
        fairSatsTotal = self.calcFairness(nrOfContactsPerSat)

        for station in stations:
            nrOfContactsPerStn.append(len(stationContacts[station.uid]))

        fairStationsTotal = self.calcFairness(nrOfContactsPerStn)

        #TO DO: update later with other weights
        self.value = (fairSatsTotal*self.satFairWt + fairStationsTotal*self.stnFairWt)/ (self.satFairWt + self.stnFairWt)
        return self.value


class dummyObjective():
    '''A dummy objective function'''
    def __init__(self, vector):
        self.vector = vector

    def evaluate(self, vector):
        return sum(vector)












        