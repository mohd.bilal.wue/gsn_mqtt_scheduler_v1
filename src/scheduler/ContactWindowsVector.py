from datetime import datetime
import logging
import json
import random

class ContactWindowsVector():
    '''This class is a list of contact windows (observations) as created from the requests.
    Hereby, data is sorted by satellite and ground station name
    '''
    def __init__(self, observations):
        '''Constructor:
        :parameter: observations are overpasses'''
        self.observations = observations
        self.startTime = 0
        self.stopTime = 0
        self.contactTime = 0
        self.satellites = []
        self.stations = []
        self.satelliteContacts = {}
        self.gsContacts = {}
        self.isTimeSorted = False
        self.generateCWV()

    def setStartTime(self, startTime):
        self.startTime = startTime

    def setStopTime(self, stopTime):
        self.stopTime = stopTime

    def setContactTime(self, contactTime):
        self.contactTime = contactTime

    def setSatellites(self, satelliteDict):
        self.satelliteContacts = satelliteDict

    def setGs(self, stationDict):
        self.gsContacts = stationDict

    def getObservations(self):
        return self.observations

    def getSatellites(self):
        return self.satellites

    def getStations(self):
        return self.stations

    def getSatelliteContacts(self):
        # empty dictionary evaluates to false
        if not bool(self.satelliteContacts):
            self.generateCWV()
        return self.satelliteContacts

    def getGsContacts(self):
        # empty dictionary evaluates to false
        if not bool(self.gsContacts):
            self.generateCWV()
        return self.gsContacts

    def generateCWV(self):
        '''
        sets totalContactTime, satelliteContactList, gsContactList
        '''
        for overpass in self.observations:
            logging.info('CW Vector: overpass {}'.format(overpass))
            if str(overpass.satellite.norad) in self.satelliteContacts:
                self.satelliteContacts[str(overpass.satellite.norad)].append(overpass)
            else:
                self.satelliteContacts[str(overpass.satellite.norad)] = [overpass]
                self.satellites.append(overpass.satellite)
            if overpass.station.uid in self.gsContacts:
                self.gsContacts[overpass.station.uid].append(overpass)
            else:
                self.gsContacts[overpass.station.uid] = [overpass]
                self.stations.append(overpass.station)
                self.contactTime += overpass.duration

    def sortByTime(self):
        '''sorts the cwVector by starting time of the overpasses'''
        # declare empty sorted variables
        satConSorted = {}
        gsConSorted = {}

        for sat in self.satelliteContacts:
            satOp = self.satelliteContacts[sat] #overpasses of this satellite
            satOpSorted = sorted(satOp, key= lambda overpass: overpass.startTime)
            satConSorted[sat] = satOpSorted

        for gs in self.gsContacts:
            gsOp = self.gsContacts[gs] #overpasses of this gs
            gsOpSorted = sorted(gsOp, key= lambda overpass: overpass.startTime)
            gsConSorted[gs] = gsOpSorted

        self.gsContacts = gsConSorted
        self.satelliteContacts = satConSorted
        self.isTimeSorted = True


    def getSatConflicts(self):
        '''finds the conflicts in the cwVector for all the satellites
        :returns a list of overpass objects that can be removed.
        '''
        removePasses = []
        if self.isTimeSorted:
            conflictPasses = {}
            for sat in self.satelliteContacts:
                overpasses = self.satelliteContacts[sat]
                conflictPasses[sat] = []
                for i in range(len(overpasses)-1):
                    if overpasses[i].endTime > overpasses[i + 1].startTime:
                        conflictPasses[sat].append((i,i+1))
                        tieBreaker = random.randint(0, 1)
                        if tieBreaker:
                            removePasses.append(overpasses[i])
                        else:
                            removePasses.append(overpasses[i+1])
        else:
            print('Error: CWVector is not sorted in time. Use sortByTime first!')
        return removePasses


    def getGsConflicts(self):
        '''finds the conflicts in the cwVector for all the ground stations
        :returns a list of overpass objects that can be removed.
        '''
        removePasses = []
        if self.isTimeSorted:
            conflictPasses = {}
            for station in self.gsContacts:
                overpasses = self.gsContacts[station]
                conflictPasses[station] = []
                for i in range(len(overpasses)-1):
                    if overpasses[i].endTime > overpasses[i + 1].startTime:
                        conflictPasses[station].append((i,i+1))
                        tieBreaker = random.randint(0, 1)
                        if tieBreaker:
                            removePasses.append(overpasses[i])
                        else:
                            removePasses.append(overpasses[i+1])
        else:
            print('CWVector is not sorted in time. Use sortByTime first!')
        return removePasses


    def resolveConflicts(self):
        '''solves conflicts randomly i.e. removes overlapping overpasses
         not working at the moment'''
        satConflicts = self.getSatConflicts()
        gsConflicts = self.getGsConflicts()
        unionConflicts = list(set().union(satConflicts, gsConflicts))
        observationsNew = self.observations
        for conflictPass in unionConflicts:
            if conflictPass in observationsNew:
                observationsNew.remove(conflictPass)

        #reassign self.observations
        self.__init__(observationsNew)
        #re-evaluate all variables o the cwVector





























