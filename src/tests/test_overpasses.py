# from observation.satellite import Satellite as Satellite1
from spaceobjects.satellite import Satellite as Satellite2
from groundobjects.groundstation import GroundStation
import numpy as np
import datetime
import math
import matplotlib.pyplot as plt

# uwe3 = Satellite2(39446, 'uwe3', '1 39446U 13066AG  19201.90417749 +.00000102 +00000-0 +20375-4 0  9990',\
#                   '2 39446 097.5811 205.4982 0068921 315.9518 043.6214 14.78398643303433')
# uwe3 = Satellite(39446, 'uwe3') #also possible
uwe4 = Satellite2(43880, 'uwe4')
t_start = datetime.datetime.utcnow() 
t_end = t_start + datetime.timedelta(days=3)
time_span = [t_start,t_end]
# latitude = 49.78778
# longitude  = 9.93611
latitude = 45.7332
longitude  = 126.627
station = GroundStation(uid = 1, name='shangdong', latitude=latitude,longitude=longitude)
overpasses  = uwe4.getOverpasses(station=station, time_span=time_span)

for overpass in overpasses:
    print("overpass {}, start_time {}, end_time {}".format(overpass, overpass.startTime, overpass.endTime))


