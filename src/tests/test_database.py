from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import datetime
import os
from database.models import Schedule
import uuid

engine = create_engine(os.getenv("TIMGSN_DB_URL_TEST"))
db = scoped_session(sessionmaker(bind=engine))

# scheduleDB = db.execute("SELECT * FROM schedule WHERE name=:name", {"name": 'abcd'}).fetchone()
# if scheduleDB is None:
#     print(True)
#
starttime = datetime.datetime.utcnow()
endtime = starttime + datetime.timedelta(days=1)
schedule = Schedule(uid=str(uuid.uuid4()), name='testSchedule', start_time=starttime, end_time=endtime, satellite_id=1, gs_id=1)
db.add(schedule)
db.commit()
db.execute("INSERT INTO schedule (uid, name, start_time, end_time, satellite_id, gs_id) VALUES (:uid, :name, :start_time, :end_time, :satellite_id, :gs_id)",
            {"uid": str(uuid.uuid4()), "name": 'testSchedule', "start_time": starttime, "end_time": endtime, "satellite_id":1, "gs_id":1})
db.commit()