from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import datetime
import json, time, os
from spaceobjects.satellite import Satellite
from groundobjects.groundstation import GroundStation
from scheduler.ContactWindowsVector import ContactWindowsVector as CWVector
from observation.tle_loader import loadTLE
from observation.overpass import Overpass
from scheduler.ObjectiveFunction import WeightedSchedulingObjective as wsObjective
from scheduler.GeneticAlgorithm import GeneticAlgorithm as ga
import logging
import matplotlib.pyplot as plt
import plotly.figure_factory as plotlyff
from database.models import Schedule
import uuid


# logging.basicConfig(level=logging.WARNING)
# logging.basicConfig(level=logging.DEBUG)


#database configuration
engine = create_engine(os.getenv("TIMGSN_DB_URL_TEST"))
db = scoped_session(sessionmaker(bind=engine))

def getRequests():
    '''
    get list of approved requests from the database. Approved implies that the receiving operator has approved the req
    :return: list of approved requests
    '''
    # fetch only the approved requests
    reqStatus = True
    requests = db.execute("SELECT * FROM trackrequests WHERE status=:status",{"status":reqStatus}).fetchall()
    return requests

def req2ContactWindows(requests):
    '''From requests create contact windows
    :parameter: list of requests
    '''
    # calculate the times
    # for test purposes let the start and end time be between now and 3 days from now
    timeVector = []
    tStart = datetime.datetime.utcnow()
    tEnd = tStart + datetime.timedelta(days=3)
    observations =[]
    for req in requests:
        satID = req.satellite_id
        gsID = req.gs_id
        satDB = db.execute("SELECT * FROM satellites WHERE id=:id", {"id":satID}).fetchone()
        gsDB = db.execute("SELECT * FROM groundstations WHERE id=:id", {"id":gsID}).fetchone()
        print("calculating overpasses for sat:{}".format(satDB.norad))
        satObj = Satellite(satDB.norad)
        gsObj = GroundStation(gsDB.uid, gsDB.gsn_name, gsDB.latitude, gsDB.longitude)
        overpasses = satObj.getOverpasses(station=gsObj, time_span=[tStart, tEnd])
        # for overpass in overpasses:
        #     print("overpass {}, start_time {}, end_time {}".format(overpass, overpass.startTime, overpass.endTime))
        if overpasses:
            for overpass in overpasses:
                observations.append(overpass)
    cwVector = CWVector(observations)
    # gsContacts = cwVector.getGsContacts()
    return cwVector

def plotSchedule(cwVector):
    '''Plot schedule
    :parameter: cwVector: vector containing all the requests'''
    observations = cwVector.observations
    plotDataGs = []
    plotDataSat = []
    for overpass in observations:
        obsDictGs = dict(Task= overpass.station.name, Start=str(overpass.startTime), Finish =str(overpass.endTime), Resource=str(overpass.satellite.norad))
        obsDictSat = dict(Task= overpass.satellite.norad, Start=str(overpass.startTime), Finish =str(overpass.endTime), Resource=str(overpass.station.name))
        plotDataGs.append(obsDictGs)
        plotDataSat.append(obsDictSat)

    figGs = plotlyff.create_gantt(plotDataGs,  index_col='Resource', show_colorbar=True, group_tasks=True)
    figGs.show()

    figSat = plotlyff.create_gantt(plotDataSat,  index_col='Resource', show_colorbar=True, group_tasks=True)
    figSat.show()


def saveSchedule2DB(overpass):
    '''saves the generated schedule to the timgsn database which can then be loaded by the web application'''
    uid = overpass.station.uid + str(overpass.satellite.norad) + str(uuid.uuid4())
    name = overpass.station.name + str(overpass.satellite.norad) + str(overpass.startTime) + str(overpass.endTime)
    starttime = str(overpass.startTime)
    endtime = str(overpass.endTime)
    satellite = db.execute("SELECT * FROM satellites WHERE norad=:norad",{"norad":overpass.satellite.norad}).fetchone()
    gs = db.execute("SELECT * FROM groundstations WHERE gsn_name=:gsn_name",{"gsn_name":overpass.station.name}).fetchone()
    schedule = Schedule(uid=uid, name=name, start_time= starttime, end_time = endtime, satellite_id=satellite.id, gs_id=gs.id, norad= satellite.norad, station_name = gs.gsn_name)
    print(schedule)
    db.add(schedule)
    db.commit()
    return schedule

def main():
    requests = getRequests()
    cwVector  = req2ContactWindows(requests)
    print('plotting schedule as requested')
    plotSchedule(cwVector)
    #
    objectiveFx = wsObjective(cwVector=cwVector, stnFairWt=0.5, satFairWt=0.5)
    nVars = len(cwVector.observations)
    gaSolver = ga(objectiveFx, popSize=50, nVars=nVars, maxGen=100)
    print('calculating optimal schedule with GA')
    population, bestSol = gaSolver.simulate()
    popHistory = gaSolver.populationHistory
    bestFitnessHistory = gaSolver.bestFitnessHistory
    avgFitnessHistory = gaSolver.averageFitnessHistory
    # print(bestSol)
    bestChromosome = bestSol[0]
    bestObservations = []
    for i in range(nVars):
        if bestChromosome[0,i]:
            bestObservations.append(cwVector.observations[i])
    cwVectorBest = CWVector(bestObservations)
    print('plotting optimal schedule with conflicts')
    plotSchedule(cwVectorBest)

    #remove conflicts explicitly
    cwVectorBest.sortByTime()
    satConflicts = cwVectorBest.getSatConflicts()
    gsConflicts = cwVectorBest.getGsConflicts()
    unionConflicts = list(set().union(satConflicts,gsConflicts))
    bestObservationsNew = bestObservations
    for conflictPass in unionConflicts:
        if conflictPass in bestObservationsNew:
            print('found conflicting pass in observations')
            bestObservationsNew.remove(conflictPass)
    cwVectorBestNew = CWVector(bestObservationsNew)

    print('plotting optimal schedule after removing conflicts')
    plotSchedule(cwVectorBestNew)

    print('testing the class conflict remover')
    cwVectorBest.resolveConflicts()
    plotSchedule(cwVectorBest)


    for overpass in bestObservationsNew:
        scheduleEntry = saveSchedule2DB(overpass)
        logging.debug(scheduleEntry)




    # # print(population)
    # print(bestSol)
    # print(bestFitnessHistory)
    # plt.figure(1)
    # plt.plot(bestFitnessHistory)
    # plt.show()
    #
    #
    # plt.figure(2)
    # plt.plot(avgFitnessHistory)
    # plt.show()

if __name__ == '__main__':
    main()
