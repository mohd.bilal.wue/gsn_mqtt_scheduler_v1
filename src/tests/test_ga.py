from scheduler.GeneticAlgorithm import GeneticAlgorithm as ga
from scheduler.ObjectiveFunction import WeightedSchedulingObjective as wtObjective
from scheduler.ObjectiveFunction import dummyObjective as dumObjective
import numpy as np
from matplotlib import pyplot as plt

vector = np.array([[1,0,0,1,0,1,0,1,1,1,0,1,0]])

objectiveFx = dumObjective(vector=vector)
gaSolver = ga(objectiveFx, popSize = 50, nVars = vector.shape[1], maxGen = 100)
population, bestSol = gaSolver.simulate()
popHistory = gaSolver.populationHistory
bestFitnessHistory = gaSolver.bestFitnessHistory
avgFitnessHistory = gaSolver.averageFitnessHistory
# print(population)
# print(bestSol)
print(bestFitnessHistory)
plt.figure(1)
plt.plot(bestFitnessHistory)
plt.show()

plt.figure(2)
plt.plot(avgFitnessHistory)
plt.show()
