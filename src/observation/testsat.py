from observation.satellite import Satellite
import numpy as np
import datetime
import math
import matplotlib.pyplot as plt

uwe3 = Satellite(39446, 'uwe3', '1 39446U 13066AG  19183.56239079  .00000135  00000-0  24546-4 0  9999', '2 39446  97.5807 188.2176 0070484  16.1151 344.2280 14.78391700300720')
# uwe3 = Satellite(39446, 'uwe3') #also possible

t_start = datetime.datetime.utcnow() 
t_end = t_start + datetime.timedelta(days=3)
dt = 5*60 #3 mins
time_vec = np.arange(0,datetime.timedelta(days=3).total_seconds(), 3.0)
azimuth_arr = np.zeros((time_vec.shape[0],1))
ele_arr = np.zeros((time_vec.shape[0],1))

i=0
crossings = []
points_crossing_all=[]
times_crossing_all = []
for t in time_vec:
    t_i = t_start + datetime.timedelta(seconds= t)
    azimuth, elevation, dist = uwe3.getStateGeodectic(49.78778, 9.93611, t_i)
    azimuth_arr[i,0] = azimuth
    ele_arr[i,0] = elevation

    if ele_arr[i,0]*ele_arr[i-1,0]<0 and i>2:
        crossings.append(i)
        points_crossing_all.append([ele_arr[i-2,0],ele_arr[i-1,0],ele_arr[i,0],ele_arr[i+1,0],ele_arr[i+2,0]])
        times_crossing_all.append([time_vec[i-2],time_vec[i-1],time_vec[i],time_vec[i+1],time_vec[i+2]])
    i+=1
#     print((i,t,elevation,azimuth))
    
print(points_crossing_all)
j=0
time_solutions = []
elevations_solutions_all =[]
for points in points_crossing_all:
    times_crossing = times_crossing_all[j]
    index = crossings[j]
    p1 = points[0]
    p2 = points[1]
    p3 = points[2]
    p4 = points[3]
    p5 = points[4]
    a0 = p2
    a1 =  (p3-p1)/2
    a2 = p1- 5*p2/2 + 2*p3 -p4/2
    a3 = (-p1 + 3*p2 - 3*p3 +p4)/2 
    P = a2/a3
    Q = a1/a3
    R = a0
    a = (3*Q-P**2)/3
    b = (2*P**3 - 9*P*Q + 27*R)/27
    delta = a**3/27 + b**2/4
    if delta>0.0:
        root1 = math.pow(-b/2 + math.pow(delta,0.5), 1/3) + math.pow(-b/2 - math.pow(delta,0.5), 1/3)
        root2 = None
        root3 = None
    elif delta==0:
        root1 = 2*math.pow(-b/2, 1.0/3)
        root2 = math.pow(b/2, 1.0/3)
        root3 = root2
    else:
        E0 = 2*math.pow(-a/3,0.5)
        cphi = -b/(2*math.pow(-a**3/27,0.5))
        sphi = math.sqrt(1-cphi**2)
        phi = math.atan2(sphi,cphi)
        Z1 = E0*math.cos(phi/3)
        Z2 = E0*math.cos(phi/3 + 3*math.pi/4)
        Z3 = E0*math.cos(phi/3 + 6*math.pi/4)
        root1 = Z1 - P/3
        root2 = Z2 - P/3
        root3 = Z3 - P/3
    
    t1 = times_crossing[0]
    t2 = times_crossing[1]
    t3 = times_crossing[2]
    t4 = times_crossing[3]
    t5 = times_crossing[4]
    a0 = t2
    a1 =  (t3-t1)/2
    a2 = t1- 5*t2/2 + 2*t3 -t4/2
    a3 = (-t1 + 3*t2 - 3*t3 +t4)/2 
    print("roots are root1 {}, root2{}, root3 {}".format(root1,root2,root3))
    if root1>=0 and root1<=1:
        solution = a3*root1**3 + a2*root1**2 + a1*root1 + a0
    elif root2>=0 and root2<=1:
        solution = a3*root2**3 + a2*root2**2 + a1*root2 + a0
    elif root3>=0 and root3<=1:
        solution = a3*root3**3 + a2*root3**2 + a1*root3 + a0
    days = solution//(24*3600)
    n = solution%(24*3600)
    hours = n//3600
    m =n%3600
    minutes = m//60
    s = n%60
    seconds = s 
    tdelta_sol = datetime.timedelta(days=int(solution/86400), hours =hours, minutes=minutes, seconds=seconds)
    time_solutions.append(t_start + tdelta_sol)
    azimuth_sol, elevation_sol, dist = uwe3.getStateGeodectic(49.78778, 9.93611, t_start +tdelta_sol)
    elevations_solutions_all.append(math.degrees(elevation_sol))
    j+=1
         
# print(azimuth)
# print(elevation)
# print(dist)
print(time_solutions)
print(elevations_solutions_all)


plt.plot(time_vec,ele_arr)
plt.show()