import urllib.request
import json

target_url = "https://www.celestrak.com/NORAD/elements/active.txt"

data = urllib.request.urlopen(target_url).readlines()


satellite = {}
satellite_list =[]
for line in data:
    line_str = line.decode().strip()
    if line_str[0]=="1" and line_str[1]==" " :
        satellite["tle_l1"]=line_str
        satellite["norad"] = line_str[2:7]
    elif line_str[0]=="2" and line_str[1]==" " :
        satellite["tle_l2"]=line_str
        satellite={}
    else:
        satellite["name"]=line_str
        satellite_list.append(satellite)
        

with open('data.json', 'w', encoding='utf-8') as outfile:  
    json.dump(satellite_list, outfile, ensure_ascii=False, indent=2)



def loadTLE(url, write_file):
    data = urllib.request.urlopen(url).readlines()
    satellite = {}
    satellite_list ={}
    for line in data:
        line_str = line.decode().strip()
        if line_str[0]=="1" and line_str[1]==" " :
            satellite["tle_l1"]=line_str
            norad =  line_str[2:7]
            satellite["norad"] = norad
        elif line_str[0]=="2" and line_str[1]==" " :
            satellite["tle_l2"]=line_str
            satellite_list[norad] = satellite
            satellite={}
        else:
            name = line_str
            satellite["name"]=name
        if write_file:  
            with open('tle_data.json', 'w', encoding='utf-8') as outfile:  
                json.dump(satellite_list, outfile, ensure_ascii=False, indent=2)
    return json.dumps(satellite_list)
