import json
import urllib.request
from sgp4.earth_gravity import wgs72
from sgp4.io import twoline2rv

 
def loadTLE(url, write_file):
    data = urllib.request.urlopen(url).readlines()
    satellite = {}
    satellite_list ={}
    for line in data:
        line_str = line.decode().strip()
        if line_str[0]=="1" and line_str[1]==" " :
            satellite["tle_l1"]=line_str
            norad =  line_str[2:7]
            satellite["norad"] = norad
        elif line_str[0]=="2" and line_str[1]==" " :
            satellite["tle_l2"]=line_str
            satellite_list[norad] = satellite
            satellite={}
        else:
            name = line_str
            satellite["name"]=name
        if write_file:  
            with open('tle_data.json', 'w', encoding='utf-8') as outfile:  
                json.dump(satellite_list, outfile, ensure_ascii=False, indent=2)
    return json.dumps(satellite_list)


tle_source ="https://www.celestrak.com/NORAD/elements/active.txt"
tle_all = json.loads(loadTLE(tle_source, False))
print(tle_all)
UWE3_sat  = tle_all["39446"]
line1 = UWE3_sat['tle_l1']
line2 = UWE3_sat['tle_l2']
satellite = twoline2rv(line1, line2, wgs72)
position,vel = satellite.propagate(2019, 7, 2, 12, 50, 19)
print(type(position))
print(position)
# print(velocity)

