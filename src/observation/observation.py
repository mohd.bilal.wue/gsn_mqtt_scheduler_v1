class Observation():
    def __int__(self, satellite, station, startTime, endTime):
        self.satellite = satellite
        self.station = station
        self.startTime = startTime
        self.endTime = endTime
        self.calcDuration()

    def calcDuration(self):
        self.duration = (self.endTime - self.startTime).total_seconds()

    def getDuration(self):
        return self.duration
