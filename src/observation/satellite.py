import json
import math
import datetime
import numpy as np
from numpy import linalg as LA
from sgp4.earth_gravity import wgs72
from sgp4.io import twoline2rv
from astrodynamics.Time import getJulianCenturies, getGMST, UTC2JD
from astrodynamics.FrameTransformations import eci2geodectic
from astrodynamics.constants import RE
from observation.tle_loader import loadTLE
from observation.overpass import Overpass


class Satellite():
    def __init__(self, norad, name='testSat', tle_l1="", tle_l2=""):
        self.norad = norad
        self.name = name
        if tle_l1=="" or tle_l2=="":
            tle_source ="https://www.celestrak.com/NORAD/elements/active.txt"
            tle_all = json.loads(loadTLE(tle_source, False))
            sat_json = tle_all[str(norad)] 
            self.tle_l1 = sat_json['tle_l1']
            self.tle_l2 = sat_json['tle_l2']
        else:    
            self.tle_l1 = tle_l1
            self.tle_l2 = tle_l2
            
    def getName(self):
        return self.name
    
    def getNorad(self):
        return self.norad    
    
    def getStateECI(self, time):
        satellite = twoline2rv(self.tle_l1, self.tle_l2, wgs72)
        position, velocity = satellite.propagate(time.year, time.month, time.day, time.hour, time.minute, time.second)
        return position, velocity
    
    def getStateGeodectic(self, latitude, longitude, time):
        ''' calculate the azimuth, elevation and range of the satellite from a given location on earth at a given time
        Reference: Vallado 3rd Edition Algorithm 15, Example 3.5 
        inputs: latitude [deg]
                longitude [deg]
                time [UTC: datetime.datetime]
        '''
        JD = UTC2JD(time) #convert utc to jd
        tUT1 = getJulianCenturies(JD) #get jd centuries
        theta_gmst_deg = getGMST(tUT1) #get GMST
        theta_lst = theta_gmst_deg + longitude #calculate local sidereal time : degrees
        theta = math.radians(theta_lst)
        phi = math.radians(latitude)
        pos, velocity = self.getStateECI(time)
        pos = np.asarray(pos).transpose()
        pos_site_ijk = np.array([RE*math.cos(phi)*math.cos(theta),RE*math.cos(phi)*math.sin(theta), RE*math.sin(phi)]).transpose()
        rho_ijk = pos - pos_site_ijk #get the position vector from the site
        rhosat_sez = eci2geodectic(rho_ijk, theta_lst, latitude)
        azimuth = math.pi - math.atan2(rhosat_sez[1], rhosat_sez[0])
        elevation = math.asin(rhosat_sez[2]/LA.norm(rhosat_sez))
        range = LA.norm(rhosat_sez)
        return math.degrees(azimuth), math.degrees(elevation), range
    
    
    def getOverpasses(self, latitude, longitude, time_span):
        t_start = time_span[0]
        t_end = time_span[1]
        t_duration = t_end - t_start 
        dt = 3*60 #3 mins
        time_vec = np.arange(0,t_duration.total_seconds(), 3.0)
        azimuth_arr = np.zeros((time_vec.shape[0],1))
        ele_arr = np.zeros((time_vec.shape[0],1))
        
        i=0
        crossings = []
        points_crossing_all=[]
        times_crossing_all = []
        sign_crossing_all = []
        for t in time_vec:
            t_i = t_start + datetime.timedelta(seconds= t)
            azimuth, elevation, dist = self.getStateGeodectic(latitude, longitude, t_i)
            azimuth_arr[i,0] = azimuth
            ele_arr[i,0] = elevation

            if ele_arr[i,0]*ele_arr[i-1,0]<0 and i>2:
                crossings.append(i)
                points_crossing_all.append([ele_arr[i-2,0],ele_arr[i-1,0],ele_arr[i,0],ele_arr[i+1,0],ele_arr[i+2,0]])
                times_crossing_all.append([time_vec[i-2],time_vec[i-1],time_vec[i],time_vec[i+1],time_vec[i+2]])
                if ele_arr[i,0]>ele_arr[i-1,0]:
                    sign_crossing_all.append(1)
                else:
                    sign_crossing_all.append(-1)
            i+=1
            j=0
        time_solutions = []
        elevations_solutions_all =[]
        azimuth_solutions_all=[]
        # at each point of crossing of the axis : solve the parabolic blending equations to find the exact point of crossing. Even higher delta_t allowed with this method
        # see vallado ed.3, C.5.3 : Blending and splining techniques
        for points in points_crossing_all:
            times_crossing = times_crossing_all[j]
            index = crossings[j]
            p1 = points[0]
            p2 = points[1]
            p3 = points[2]
            p4 = points[3]
            p5 = points[4]
            a0 = p2
            a1 =  (p3-p1)/2
            a2 = p1- 5*p2/2 + 2*p3 -p4/2
            a3 = (-p1 + 3*p2 - 3*p3 +p4)/2 
            P = a2/a3
            Q = a1/a3
            R = a0
            a = (3*Q-P**2)/3
            b = (2*P**3 - 9*P*Q + 27*R)/27
            delta = a**3/27 + b**2/4
            if delta>0.0:
                root1 = math.pow(-b/2 + math.pow(delta,0.5), 1/3) + math.pow(-b/2 - math.pow(delta,0.5), 1/3)
                root2 = None
                root3 = None
            elif delta==0:
                root1 = 2*math.pow(-b/2, 1.0/3)
                root2 = math.pow(b/2, 1.0/3)
                root3 = root2
            else:
                E0 = 2*math.pow(-a/3,0.5)
                cphi = -b/(2*math.pow(-a**3/27,0.5))
                sphi = math.sqrt(1-cphi**2)
                phi = math.atan2(sphi,cphi)
                Z1 = E0*math.cos(phi/3)
                Z2 = E0*math.cos(phi/3 + 3*math.pi/4)
                Z3 = E0*math.cos(phi/3 + 6*math.pi/4)
                root1 = Z1 - P/3
                root2 = Z2 - P/3
                root3 = Z3 - P/3
            
            t1 = times_crossing[0]
            t2 = times_crossing[1]
            t3 = times_crossing[2]
            t4 = times_crossing[3]
            t5 = times_crossing[4]
            a0 = t2
            a1 =  (t3-t1)/2
            a2 = t1- 5*t2/2 + 2*t3 -t4/2
            a3 = (-t1 + 3*t2 - 3*t3 +t4)/2 
            if root1>=0 and root1<=1:
                solution = a3*root1**3 + a2*root1**2 + a1*root1 + a0
            elif root2>=0 and root2<=1:
                solution = a3*root2**3 + a2*root2**2 + a1*root2 + a0
            elif root3>=0 and root3<=1:
                solution = a3*root3**3 + a2*root3**2 + a1*root3 + a0
            days = solution//(24*3600)
            n = solution%(24*3600)
            hours = n//3600
            m =n%3600
            minutes = m//60
            s = n%60
            seconds = s 
            tdelta_sol = datetime.timedelta(days=days, hours =hours, minutes=minutes, seconds=seconds)
            time_solutions.append(t_start + tdelta_sol)
            azimuth_sol, elevation_sol, dist = self.getStateGeodectic(latitude, longitude, t_start +tdelta_sol)
            elevations_solutions_all.append(elevation_sol)
            azimuth_solutions_all.append(azimuth_sol)           
            j+=1
        
        
        overpasses_all = []
        
        if sign_crossing_all[0]==-1:
            index_start=1
        else:
            index_start =0
        ind =index_start
        for pass_index in range(index_start, int(len(time_solutions)/2)):
            start_time_op = time_solutions[ind]
            end_time_op = time_solutions[ind+1]
            az_start_pass, ele_start_pass, dist_start_pass = self.getStateGeodectic(latitude, longitude, start_time_op)
            az_end_pass, ele_end_pass, dist_end_pass = self.getStateGeodectic(latitude, longitude, end_time_op)
            overpass_i = Overpass(self, latitude, longitude, start_time_op, end_time_op, ele_start_pass, ele_end_pass, visibility=True)
            ind+=2
            overpasses_all.append(overpass_i)
        
        return overpasses_all
