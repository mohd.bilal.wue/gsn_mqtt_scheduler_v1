import uuid
class Overpass():
    
    def __init__(self, satellite, station, start_time, end_time, ele_start, ele_end, visibility=False):
        self.uid = str(uuid.uuid4())
        self.station = station
        self.satellite = satellite
        self.startTime = start_time
        self.endTime = end_time
        self.visibility = visibility
        self.ele_start = ele_start
        self.ele_end = ele_end
        self.calcDuration()

    def calcDuration(self):
        self.duration = (self.endTime - self.startTime).total_seconds()

    def getDuration(self):
        return self.duration

    def interpolate(self):
        return None

    def getUID(self):
        return self.uid
        
        