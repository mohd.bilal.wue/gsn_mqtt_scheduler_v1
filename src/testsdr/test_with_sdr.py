from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient as AWS    #To connect to AWS IOT
import json, time, os
import threading
from testsdr import Sdr
from multiprocessing.pool import ThreadPool


topics_pub = ['sdrtest']
topics_sub = ['schedule']


myAWS = AWS("sdrtest")
enpoint = "a23fpg12m7w3rv-ats.iot.eu-central-1.amazonaws.com"
portAWS = 8883
myAWS.configureEndpoint(enpoint, portAWS)
myAWS.configureCredentials("AmazonRootCA1.pem", "04666a15a4-private.pem.key", "04666a15a4-certificate.pem.crt")
print("aws configured...")


#Starts connection to AWS
myAWS.connect()
print("aws connected...")

# set up sdr
sample_rate = 2.4e6
center_freq = 100e6
gain = 25
frame_len = 2056
read_mode = "sampling"
sdr = Sdr.SdrReader(sample_rate=sample_rate, center_freq=center_freq, gain=gain, frame_len=frame_len, read_mode=read_mode)


#callback sends data to client side
def alertCallback(client, userdata, message):
    #receive the alert from webapp; process the info
    print("received {} on topic {}".format(message.payload, message.topic))
    topicdata = json.loads(message.payload)
    print(topicdata)
    #start tracking
    norad= topicdata["norad"] 
    start_time = topicdata["start_time"] 
    end_time = topicdata["end_time"] 
    
    #start tracking
    
#subscribe to topic with callback as events
myAWS.subscribe(topics_sub[0], 1, alertCallback)


def sdrStart():
    global sdr
    pool = ThreadPool(processes=1)
    async_res = pool.apply_async(sdr.readSample())
    samples =  async_res.get()
    print(samples)
    
    
    
    
    
    
    
    