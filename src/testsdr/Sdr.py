import threading
import sys
from rtlsdr import RtlSdr
from scipy import fftpack
import matplotlib.pyplot as plt
import numpy as np
import math
from math import log
import json
from time import sleep


class SdrReader(threading.Thread):
    
    def __init__(self, sample_rate, center_freq, gain, frame_len, read_mode):
        threading.Thread.__init__(self)
        self.sdr = RtlSdr()
        self.sdr.sample_rate = sample_rate
        self.sdr.center_freq = center_freq
        self.sdr.gain = gain
        self.frame_len = frame_len
        self.read_mode = read_mode
                
    def run(self):
        print("reading data")

                       
    def readSample(self):
        try:
            samples = self.sdr.read_samples(self.frame_len)
            return samples
        except IOError:
            print("Could not read samples. Check if device is inserted")
        finally:
            self.sdr.close()
    
    def readStream(self):
        while True:
            try:
                samples = self.sdr.read_samples(self.frame_len)
                #log data into database here
                yield samples
            except IOError:
                print("Could not read samples. Check if device is inserted")
        self.sdr.close()
        print("radio closed.")
        
    def readPsdStream(self):
        while True:
            try:
                samples = self.sdr.read_samples(self.frame_len)
                pxx, freqs = plt.psd(samples, NFFT=1024, Fs=self.sdr.sample_rate, Fc=self.sdr.center_freq)
#                 sxx, freqSxx, timeBins,im = plt.specgram(samples, NFFT=1024, Fs=self.sdr.sample_rate, Fc=self.sdr.center_freq)
                print("calculated psd") 
                yield "data: {} \n\n".format(json.dumps({"freq":freqs.tolist(), "power":pxx.tolist()}))
#                 yield "data: {} \n\n".format(json.dumps({"spectrum":sxx.tolist(), "freqSxx":freqSxx.tolist(),"time": timeBins.tolist()}))
#                 yield "data: {} \n\n".format(json.dumps({"freq":freqs.tolist(), "power":pxx.tolist(), "spectrum":sxx.tolist(), "freqSxx":freqSxx.tolist(),"time": timeBins.tolist()}))
                sleep(1)
            except IOError:
                print("Could not read samples. Check if device is inserted")
                self.sdr.close()
                break
        self.sdr.close()
        print("radio closed.")
                
    def setSampleRate(self, rate):
        self.sdr.set_sample_rate(rate)
    
    def setCenterFreq(self, freq):
        self.sdr.set_center_freq(freq)
    
    def setGain(self, gain):
        self.sdr.set_gain(gain)
    
# 
# 
# def main():
#     sample_rate = 2.4e6
#     center_freq = 100e6
#     gain = 25
#     frame_len = 2056
#     read_mode = "sampling"
#     sdr = SdrReader(sample_rate=sample_rate, center_freq=center_freq, gain=gain, frame_len=frame_len, read_mode=read_mode)
#     
#     sdr.start()
#     plt.ion()
#     fig = plt.figure()
#     ax = fig.add_subplot(111)
# 
# 
#     for sample in sdr.readStream():
#         y = fftpack.fft(sample) 
#         ax.psd(sample, NFFT=1024, Fs= sample_rate, Fc= center_freq)
#         fig.canvas.draw()  
#         fig.canvas.flush_events()
#         ax.clear()
#           
#     plt.show()
#       
#         
#     
#     if sys.exit():
#         sdr.sdr.close()
#         
# if __name__=="__main__":
#     main()
#     