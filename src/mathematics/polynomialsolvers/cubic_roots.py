import math


def cubic_roots(P, Q, R):
    """Solve a cubic equation of the form y(x) = x^3 + Px^2 + Qx + R.
    Currently only real roots are returned. Imaginary roots are set to None.
    Returns a list of roots.
    """
    a = (3 * Q - P ** 2) / 3
    b = (2 * P ** 3 - 9 * P * Q + 27 * R) / 27
    delta = a ** 3 / 27 + b ** 2 / 4

    # delta allows finding the type of roots.
    # If delta>0, there is one real root and two imaginary roots which we do not report here
    # If delta=0, all three roots are real.
    if delta > 0.0:
        root1 = math.pow(-b / 2 + math.sqrt(delta), 1 / 3) + math.pow(-b / 2 - math.sqrt(delta), 1 / 3)
        root2 = None
        root3 = None
    elif delta == 0:
        root1 = 2 * math.pow(-b / 2, 1.0 / 3)
        root2 = math.pow(b / 2, 1.0 / 3)
        root3 = root2
    else:
        E0 = 2 * math.pow(-a / 3, 0.5)
        cphi = -b / (2 * math.pow(-a ** 3 / 27, 0.5))
        sphi = math.sqrt(1 - cphi ** 2)
        phi = math.atan2(sphi, cphi)
        Z1 = E0 * math.cos(phi / 3)
        Z2 = E0 * math.cos(phi / 3 + 3 * math.pi / 4)
        Z3 = E0 * math.cos(phi / 3 + 6 * math.pi / 4)
        root1 = Z1 - P / 3
        root2 = Z2 - P / 3
        root3 = Z3 - P / 3

    return [root1, root2, root3]
