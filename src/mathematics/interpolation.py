"""
Created on Jul 18, 2019

@author: mob33uk
"""
import math
from mathematics.geometry import Point
from mathematics.polynomialsolvers.cubic_roots import cubic_roots


def parabolic(p1, p2, p3, p4):
    """ This method interpolates for a value at a given x coordinate given 4 points.
    The four points must be consecutive but need not be uniformly spaced.
    Refer Vallado Appendix C, parabolic blending
    """
    # Consider 3rd order eqn for the curve: f(t) = a3*t^3 + a2*t^2 + a1*t + a0
    # the derivative of f(t) is then: 3a3*t^2 + 2*a2*t^2 + a1
    # step 1: calculate the coefficients of the 3rd order equation using the y-component of the points 
    a0 = p2.y
    a1 = (p3.y - p1.y) / 2
    a2 = p1.y - 5 * p2.y / 2 + 2 * p3.y - p4.y / 2
    a3 = (-p1.y + 3 * p2.y - 3 * p3.y + p4.y) / 2

    # solve the cubic equation to get the crossing point of the blending curve
    # step 1: normalise the equation for coefficient of highest power to 1 i.e. eqn becomes: x^3+Px^2+Qx+R
    P = a2 / a3
    Q = a1 / a3
    R = a0
    # step2: calculate roots
    roots_fc = cubic_roots(P, Q, R)
    root1 = roots_fc[0]
    root2 = roots_fc[1]
    root3 = roots_fc[2]

    # step3: find new coefficients using the time values, and then choose real roots from the step2 roots
    t1 = p1.x
    t2 = p2.x
    t3 = p3.x
    t4 = p4.x
    a0 = t2
    a1 = (t3 - t1) / 2
    a2 = t1 - 5 * t2 / 2 + 2 * t3 - t4 / 2
    a3 = (-t1 + 3 * t2 - 3 * t3 + t4) / 2
    if roots_fc[0] >= 0 and roots_fc[0] <= 1:
        solution = a3 * root1 ** 3 + a2 * root1 ** 2 + a1 * root1 + a0
    elif root2 >= 0 and root2 <= 1:
        solution = a3 * root2 ** 3 + a2 * root2 ** 2 + a1 * root2 + a0
    elif root3 >= 0 and root3 <= 1:
        solution = a3 * root3 ** 3 + a2 * root3 ** 2 + a1 * root3 + a0

    return solution
