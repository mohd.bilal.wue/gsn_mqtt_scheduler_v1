'''
Created on Jul 18, 2019

@author: mob33uk
'''
class Point():
    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z
        
    def setX(self, x):
        self.x = x
    
    def setY(self,y):
        self.y = y
    
    def setZ(self,z):
        self.z = z
    
    